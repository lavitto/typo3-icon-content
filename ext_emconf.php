<?php
/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

/**
 * Extension Manager/Repository config file for ext "icon_content".
 */

/** @noinspection PhpUndefinedVariableInspection */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Icon content',
    'description' => 'Adds an icon content element to the website. The content element adds an icon of the Font Awesome Library instead of an image.',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'php' => '7.1.0-7.4.99'
        ],
        'conflicts' => [],
        'suggests' => [
        ]
    ],
    'autoload' => [
        'psr-4' => [
            'Lavitto\\IconContent\\' => 'Classes'
        ],
    ],
    'state' => 'beta',
    'author' => 'Philipp Mueller',
    'author_email' => 'philipp.mueller@lavitto.ch',
    'author_company' => 'lavitto ag',
    'version' => '2.0.0',
];
