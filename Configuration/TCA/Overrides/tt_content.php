<?php
/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

/** @noinspection PhpFullyQualifiedNameUsageInspection */
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    [
        'LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:iconcontent_iconcontent',
        'iconcontent_iconcontent',
        'EXT:icon_content/Resources/Public/Icons/ContentElements/iconcontent_iconcontent.svg'
    ],
    'CType',
    'iconcontent'
);

/**
 * Palettes
 */
$GLOBALS['TCA']['tt_content']['palettes']['iconcontent_icons'] = [
    'label' => 'LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:tt_content.iconcontent_iconcontent.icon',
    'showitem' => 'pi_flexform;LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:tt_content.iconcontent_iconcontent.icon'
];

$GLOBALS['TCA']['tt_content']['palettes']['iconcontent_appearance'] = [
    'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance',
    'showitem' => 'imageorient, imageborder'
];

/**
 * CType iconcontent_iconcontent
 */
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds']['*,iconcontent_iconcontent'] = 'FILE:EXT:icon_content/Configuration/FlexForm/IconContent.xml';
$GLOBALS['TCA']['tt_content']['types']['iconcontent_iconcontent'] = [
    'columnsOverrides' => $GLOBALS['TCA']['tt_content']['types']['textmedia']['columnsOverrides'],
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
            --palette--;;headers,bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
        --div--;LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:tt_content.iconcontent_iconcontent.icon,
            --palette--;;iconcontent_icons,
            --palette--;;iconcontent_appearance,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;;frames,
            --palette--;;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
        --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,'
];

