<?php
/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:icon_content/Configuration/TSconfig/page.tsconfig">
');

$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1570440243] = [
    'nodeName' => 'lavittoIconContentElement',
    'priority' => 40,
    'class' => \Lavitto\IconContent\Form\Element\IconContentElement::class
];
