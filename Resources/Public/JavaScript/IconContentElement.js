define(['jquery'], function () {
	let IconContentElement = {};

	IconContentElement.init = function () {
		$('.ext-icon-content').each(function () {
			let extIconContent = $(this);
			let hiddenField = extIconContent.find('input[type=hidden]');
			let items = extIconContent.find('.item');
			items.on('click', function () {
				items.removeClass('active');
				let item = $(this);
				hiddenField.val(item.data('value'));
				item.addClass('active');
			});
		});

		$('.ext-icon-content input[name=icon-filter]').on('keyup', function () {
			IconContentElement.updateIconList($(this).val());
		});

		$('.ext-icon-content button.reset-icon-filter').on('click', function () {
			IconContentElement.resetFilter();
		});
	};

	IconContentElement.resetFilter = function () {
		$('.ext-icon-content input[name=icon-filter]').val('');
		IconContentElement.updateIconList('');
	};

	IconContentElement.updateIconList = function (searchWord) {
		$('.ext-icon-content .icon-list .item').each(function () {
			if (searchWord === '' || $(this).data('keywords').toLowerCase().indexOf(searchWord.toLowerCase()) >= 0) {
				$(this).removeClass('hidden');
			} else {
				$(this).addClass('hidden');
			}
		});

		$('.ext-icon-content .icon-list .row').each(function () {
			let empty = $(this).find('.empty');
			if ($(this).find('.item').length === $(this).find('.item.hidden').length) {
				empty.removeClass('hidden');
			} else {
				empty.addClass('hidden');
			}
		});
	};

	IconContentElement.init();
	return IconContentElement;
});
